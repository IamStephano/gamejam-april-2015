﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{

	public GameObject aboutImage;
	public bool aboutShown;

	// Use this for initialization

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) && aboutShown)
		{
			aboutImage.GetComponent<Animation>().Play("AboutOUT");
			aboutShown = false;
		}
	}
	public void newGame()
	{
		Application.LoadLevel("Game");
	}

	public void about()
	{
		aboutShown = true;
		aboutImage.GetComponent<Animation>().Play("AboutIN");
	}

	public void exit()
	{
		Application.Quit();
	}
}
