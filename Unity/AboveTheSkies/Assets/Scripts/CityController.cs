﻿using System;
using System.Resources;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CityController : MonoBehaviour {

	public ResourceContainer resManager;
	public Texture2D buildCursor;

	public GameObject cityPanel;
	public GameObject player;

	//Buttons
	public Button repairButton;
	public Button foodButton;
	public Button cannonButton;
	public Button speedButton;
	public Button hullButton;

    private float openMenu;
    private int timeToOpen = 5;
    private bool openShop = true;


	void Start()
	{
		resManager = GameObject.Find("GameController").GetComponent<ResourceContainer>();
	}

	void FixedUpdate()
	{
		if (resManager.currentStorage >= resManager.maxStorage)
		{
			foodButton.interactable = false;
		}
		if (resManager.shipCannonLevel == 5)
		{
			cannonButton.interactable = false;
		}
		if (resManager.shipSpeedLevel == 5)
		{
			speedButton.interactable = false;
		}
	}

	void OnTriggerEnter(Collider other)
	{
	    if (other.tag == "Player")
	    {
	        openMenu = timeToOpen;
	    }
	}

    void OnTriggerStay(Collider other)
    {
        Debug.Log(timeToOpen);
        if (other.tag == "Player"&&openShop==true)
        {
            openMenu =- Time.deltaTime;
            if (openMenu <= 0)
            {
                cityPanel.SetActive(true);
                repairCheck();
                openShop = false;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        openShop = true;
        cityPanel.SetActive(false);
    }

	public void exit()
	{
		cityPanel.SetActive(false);
	}

	public void repairCheck()
	{
		if (player.GetComponent<PlayerBehaviour>().health < player.GetComponent<PlayerBehaviour>().maxHealth)
		{
			Debug.Log("Repair Available");
			repairButton.interactable = true;
		}
	}

	Boolean priceCheck(int ore, int lumber, int wool, int gem, int gold)
	{
		Debug.Log("Running Price Check");
		if (resManager.playerOre >= ore
		    && resManager.playerLumber >= lumber
		    && resManager.playerWool >= wool
		    && resManager.playerGems >= gem
		    && resManager.playerGold >= gold)
		{
			Debug.Log("Can pay");
			return true;	
		}
		else
		{
			return false;
		}

		
			
	}

	public void Repair()
	{
		int ore = 1;
		int lumber = 1;
		int wool = 1;
		Debug.Log(priceCheck(ore, lumber, wool, 0, 0));
		if (priceCheck(ore, lumber, wool, 0, 0))
		{
			Debug.Log("Pay Repair");
			resManager.UpdateResources(-ore, -lumber, -wool, 0, 0);
			player.GetComponent<PlayerBehaviour>().health = 100;
			repairButton.interactable = false;
		}
	}
    public void Food()
    {
        int gold = 10;
        Debug.Log(priceCheck(0, 0, 0, 0, gold));
	    if (priceCheck(0, 0, 0, 0, gold))
	    {
			Debug.Log("Payed");
			resManager.playerGold -= 10;
			resManager.playerFood++;    
	    }
            
    }
    public void Spyglass()
    {
        int ore = 0;
        int lumber = 0;
        int wool = 0;
        int gem = 3;
        int gold = 150;
        if (priceCheck(ore, lumber, wool, gem, gold))
        {
            resManager.playerGold -= gold;
            resManager.playerGems -= gem;
            resManager.spyglass = true;
        }
    }
    public void ShipSpeed()
    {
        int level = resManager.shipSpeedLevel;
        int ore = 0;
        int lumber = 0;
        int wool = 4*level;
        int gem = 0;
        int gold = 20*level;
        if (priceCheck(ore, lumber, wool, gem, gold))
        {
            resManager.playerOre -= ore;
            resManager.playerLumber -= lumber;
            resManager.playerWool -= wool;
            resManager.playerGems -= gem;
            resManager.playerGold -= gold;
            resManager.shipSpeedLevel++;
        }
    }
    public void ShipStorage()
    {
        int level = resManager.shipStorageLevel;
        int ore = 0;
        int lumber = 4*level;
        int wool = 4 * level;
        int gem = 0;
        int gold = 15 * level;
        if (priceCheck(ore, lumber, wool, gem, gold))
        {
            resManager.playerOre -= ore;
            resManager.playerLumber -= lumber;
            resManager.playerWool -= wool;
            resManager.playerGems -= gem;
            resManager.playerGold -= gold;
            resManager.shipStorageLevel++;
        }
    }
    public void ShipHull()
    {
        int level = resManager.shipHullLevel;
        int ore = 2*level;
        int lumber = 2*level;
        int wool = 0;
        int gem = 0;
        int gold = 15 * level;
        if (priceCheck(ore, lumber, wool, gem, gold))
        {
            resManager.playerOre -= ore;
            resManager.playerLumber -= lumber;
            resManager.playerWool -= wool;
            resManager.playerGems -= gem;
            resManager.playerGold -= gold;
            resManager.shipHullLevel++;
        }
    }
    public void ShipCannon()
    {
        int level = resManager.shipCannonLevel;
        int ore = 5*level;
        int lumber = 0;
        int wool = 0;
        int gem = 0;
        int gold = 25 * level;
        if (priceCheck(ore, lumber, wool, gem, gold))
        {
            resManager.playerOre -= ore;
            resManager.playerLumber -= lumber;
            resManager.playerWool -= wool;
            resManager.playerGems -= gem;
            resManager.playerGold -= gold;
            resManager.shipCannonLevel++;
			player.GetComponent<PlayerBehaviour>().addCannon();
        }
    }
    public void Ore()
    {
        int worth = 25;
        if (resManager.playerOre > 0)
        {
            resManager.playerOre--;
            resManager.playerGold += worth;
        }
    }
    public void Lumber()
    {
        int worth = 15;
        if (resManager.playerLumber > 0)
        {
            resManager.playerLumber--;
            resManager.playerGold += worth;
        }
    }
    public void Wool()
    {
        int worth = 5;
        if (resManager.playerWool > 0)
        {
            resManager.playerWool--;
            resManager.playerGold += worth;
        }
    }
    public void Gem()
    {
        int worth = 50;
        if (resManager.playerGems > 0)
        {
            resManager.playerGems--;
            resManager.playerGold += worth;
        }
    }
}
