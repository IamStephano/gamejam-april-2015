﻿using System;
using UnityEngine;
using System.Collections;
using System.Threading;

public class BuildController : MonoBehaviour
{
    public int goldCost;
    public int gemCost;
    public int lumberCost;
    public int woolCost;
    public int oreCost;
	public GameObject unBuild;
	public GameObject build;
	public Boolean isBuild;
	public Texture2D buildCursor;
	public Boolean canBuild;
    public GameObject resManager;

	public GameObject priceBox;

    void Start()
    {
        resManager = GameObject.Find("GameController");
    }

	void OnMouseEnter()
	{
		if (!isBuild)
		{
			canBuild = true;
			unBuild.GetComponent<Renderer>().material.color = Color.black;
			priceBox.SetActive(true);
			Cursor.SetCursor(buildCursor, Vector2.zero, CursorMode.Auto);
		}	
	}

    void OnMouseExit()
	{
		if (!isBuild)
		{
			canBuild = false;
			unBuild.GetComponent<Renderer>().material.color = Color.white;
			priceBox.SetActive(false);
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
		}
	}

	void OnMouseDown()
	{
		if (canBuild&&!isBuild)
		{
		    if (Vector3.Distance(transform.position, GameObject.FindGameObjectWithTag("GameController").transform.position) < 20)
		    {
                if (resManager.GetComponent<ResourceContainer>().playerGold >= goldCost & resManager.GetComponent<ResourceContainer>().playerGems >= gemCost & resManager.GetComponent<ResourceContainer>().playerOre >= oreCost & resManager.GetComponent<ResourceContainer>().playerWool >= woolCost & resManager.GetComponent<ResourceContainer>().playerLumber >= lumberCost)
                {
                    unBuild.SetActive(false);
                    build.SetActive(true);
                    isBuild = true;
                    resManager.GetComponent<ResourceContainer>().playerGold -= goldCost;
                    resManager.GetComponent<ResourceContainer>().playerGems -= gemCost;
                    resManager.GetComponent<ResourceContainer>().playerOre -= oreCost;
                    resManager.GetComponent<ResourceContainer>().playerLumber -= lumberCost;
                    resManager.GetComponent<ResourceContainer>().playerWool -= woolCost;
					priceBox.SetActive(false);
                }
		    }
		}
	}
}