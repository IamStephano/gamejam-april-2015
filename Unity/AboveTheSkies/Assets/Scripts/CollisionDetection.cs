﻿using System.Threading;
using UnityEngine;
using System.Collections;

public class CollisionDetection : MonoBehaviour
{

	private GameObject player;
	private float hitTimer = 0;
	public AudioClip crash;

	void Start()
	{
		player = GameObject.Find("Player");
	}
	void OnCollisionEnter(Collision other)
	{
		Debug.Log("collision");
		if (other.gameObject.tag == "Island" && hitTimer < 1)
		{
			player.GetComponent<PlayerBehaviour>().health -= 10;
			player.GetComponent<AudioSource>().PlayOneShot(crash);
			hitTimer = 3f;
		}
	}

	void Update()
	{
		if (hitTimer > 0)
		{
			hitTimer -= Time.deltaTime;
		}
	}
}
