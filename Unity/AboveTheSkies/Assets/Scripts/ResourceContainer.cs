﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResourceContainer : MonoBehaviour
{
	//Variables
	public int currentStorage;
	public int maxStorage = 15;
    private int storagePrLevel = 15;
    public int currentRoom;
	
	//Items in storage
	public int storedLumber;
	public int storedOre;
	public int storedGems;
	public int storedWool;
    public int storedGold;

	//Items on player
	public int playerLumber;
	public int playerOre;
	public int playerGems;
	public int playerWool;
    public int playerGold;
	public int playerGolda;
	public int playerFood;

	//UI Elements
	public Text lumberText;
	public Text oreText;
	public Text gemsText;
	public Text woolText;
	public Text goldText;
	public Text foodText;

    //Level/unique items
    public bool spyglass = false;
    public int shipSpeedLevel = 1;
    public int shipStorageLevel = 1;
    public int shipHullLevel = 1;
    public int shipCannonLevel = 1;


	void Update()
	{
	    maxStorage = storagePrLevel*shipStorageLevel;
		currentStorage = playerFood + playerGems + playerLumber + playerOre + playerWool;
		lumberText.text = playerLumber.ToString();
		oreText.text = playerOre.ToString();
		gemsText.text = playerGems.ToString();
		woolText.text = playerWool.ToString();
		goldText.text = playerGold.ToString();
		foodText.text = playerFood.ToString();
	    currentRoom = maxStorage - currentStorage;

	}

    public void GetRes(int ore, int lumber, int wool, int gem, int food)
    {
        if (currentStorage > 0)
        {
            playerOre += ore;
            playerLumber += lumber;
            playerWool += wool;
            playerGems += gem;
            playerGold += food;
        }
    }


	public void UpdateResources(int ore, int lumber, int wool, int gem, int gold)
	{
		playerOre += ore;
		playerLumber += lumber;
		playerWool += wool;
		playerGems += gem;
		playerGold += gold;
	}


}
