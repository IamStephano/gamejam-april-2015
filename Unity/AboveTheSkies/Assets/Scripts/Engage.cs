﻿using UnityEngine;
using System.Collections;

public class Engage : MonoBehaviour
{

	public Texture2D engageIcon;
	public GameObject player;
	public Animation reloadAnim;

	// Use this for initialization
	private void OnMouseEnter()
	{
		Cursor.SetCursor(engageIcon, Vector2.zero, CursorMode.Auto);
	}

	private void OnMouseExit()
	{
		Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
	}

	private void OnMouseDown()
	{
		player.GetComponent<PlayerBehaviour>().inCombat = true;
		player.GetComponent<PlayerBehaviour>().enemy = gameObject;
		Debug.Log("Playing Animation");
	}
}