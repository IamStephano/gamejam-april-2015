﻿using UnityEngine;
using System.Collections;

public class MovementController : MonoBehaviour
{

	public float translation;
	public float rotation;
	public float rotationSpeed;
	public float translationSpeed;

	public ResourceContainer resManager;

	void Update ()
	{
		rotation = Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed;
		translation = Input.GetAxis("Vertical") * Time.deltaTime * translationSpeed;
		GetComponent<Rigidbody>().AddForce(transform.forward * translation, ForceMode.Force);
		transform.Rotate(0, rotation, 0);
		if (Input.GetKey(KeyCode.E))
		{
			GetComponent<Rigidbody>().AddForce(transform.right * translationSpeed * Time.deltaTime, ForceMode.Force);	
		}
		if (Input.GetKey(KeyCode.Q))
		{
			GetComponent<Rigidbody>().AddForce(-transform.right * translationSpeed * Time.deltaTime, ForceMode.Force);
		}
	}

	public void UpdateShipSpeed()
	{
		translationSpeed = 35*((resManager.shipSpeedLevel/10) + 1);
	}
}
