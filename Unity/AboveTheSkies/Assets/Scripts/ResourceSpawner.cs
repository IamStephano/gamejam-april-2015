﻿using UnityEngine;
using System.Collections;

public class ResourceSpawner : MonoBehaviour
{

    public int timer;
	public string resource;
    public int res;
    public int room;

    private float timeLeft;
    private bool fullCapacity = false;

	public GameObject resText;

	// Use this for initialization
	void Start ()
	{
	    timeLeft = timer;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (fullCapacity)
	    {
            Debug.Log("UI should be added");
	    }
	    else
	    {
            timeLeft -= Time.deltaTime;
            if (timeLeft <= 0)
            {
                timeLeft += timer;
                res++;
	            
                if (res == room)
                {
                    fullCapacity = true;
                }
            }
	    }
		resText.GetComponent<TextMesh>().text = res.ToString();
	}
}
