﻿using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour
{
	public float speed = 10;
	public Transform target;


	void Update()
	{
		if (Input.GetMouseButton(0))
		{
			transform.RotateAround(target.position, Vector3.up, Input.GetAxis("Mouse X") * speed);

		}
	}
}
