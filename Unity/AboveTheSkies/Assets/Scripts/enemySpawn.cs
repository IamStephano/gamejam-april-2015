﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class enemySpawn : MonoBehaviour {


	public List<Transform> piratesAlive = new List<Transform>();
	public Transform[] spawns;

	public Transform waypoint1;
	public Transform waypoint2;
	public Transform waypoint3;

	public GameObject pirate;

	void Start()
	{
		InvokeRepeating("spawnEnemy", 10f, 10f);	
	}

	void spawnEnemy()
	{
		if (piratesAlive.Count < 3)
		{
			int randomNumber = Random.Range(1, 3);
			switch (randomNumber)
			{
				case 1:
					GameObject enemy1 = Instantiate(pirate, spawns[0].position, spawns[0].rotation) as GameObject;
					enemy1.GetComponent<EnemyBehaviour>().currentWaypoint = waypoint1;
					piratesAlive.Add(enemy1.transform);
					break;
				case 2:
					GameObject enemy2 = Instantiate(pirate, spawns[1].position, spawns[1].rotation) as GameObject;
					enemy2.GetComponent<EnemyBehaviour>().currentWaypoint = waypoint2;
					piratesAlive.Add(enemy2.transform);
					break;
				case 3:
					GameObject enemy3 = Instantiate(pirate, spawns[2].position, spawns[2].rotation) as GameObject;
					enemy3.GetComponent<EnemyBehaviour>().currentWaypoint = waypoint3;
					piratesAlive.Add(enemy3.transform);
					break;
			}
		}
	}
}
