﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityStandardAssets.Effects;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class PlayerBehaviour : MonoBehaviour
{

	public float health = 100;
	public float maxHealth = 100;

	public bool docked;
	private Vector3 lastPos;

	public GameObject playerCamera;
	public float dockTimer;

	public bool inCombat;
	public GameObject enemy;

	public float shootTimer = 5f;

	public GameObject combatIcon;
	public GameObject smallExplosion;
	public GameObject ball;

	public List<Transform> Cannons = new List<Transform>();

	public GameObject reloadAnim;
	public GameObject reload;

	public Image healthBar;

	public bool firstReload = false;

	public AudioClip combatBG;
	public AudioClip defaultBG;
	public AudioClip cannonSound;

	public ResourceContainer resManager;

	//Cannons
	public Transform defaultCannon;
	public List<Transform> availableCannons = new List<Transform>();

	public GameObject explosion;

	void Start()
	{
		GetComponent<AudioSource>().clip = defaultBG;
		GetComponent<AudioSource>().Play();
		lastPos = transform.position;

	}

	void Update()
	{
		healthBar.rectTransform.sizeDelta = new Vector2((health*2.2f), 29);
		if (inCombat)
		{
			reload.SetActive(true);
			if (!firstReload)
			{
				GetComponent<AudioSource>().clip = combatBG;
				GetComponent<AudioSource>().Play();
				reloadAnim.GetComponent<Animation>().Play();		
				firstReload = true;
			}

			combatIcon.SetActive(true);
			float distance = Vector3.Distance(enemy.transform.position, transform.position);
			if (shootTimer <= 0f)
			{
				if (Input.GetKeyDown(KeyCode.Space) && distance < 200)
				{
					shootTimer = 5f;
					Fire();
				}
				
			}
			else
			{
				shootTimer -= Time.deltaTime;	
			}
			if (enemy.GetComponent<EnemyBehaviour>().health <= 0)
			{
				inCombat = false;
				StartCoroutine(endCombat());
			}
		}
		else
		{
			combatIcon.SetActive(false);
			reload.SetActive(false);
			firstReload = false;
		}
		if (health <= 0)
		{
			explosion.SetActive(true);
			transform.Translate(-Vector3.up * Time.deltaTime * 5, Space.World);
			transform.Rotate(1, .3f, .1f);
			StartCoroutine("Restart");
		}
	}

	Boolean playerMoving()
	{
		Vector3 displacement = transform.position - lastPos;
		lastPos = transform.position;
		return displacement.magnitude > 0.01;
	}

	void Fire()
	{
		Vector3 relativePoint = transform.InverseTransformPoint(enemy.transform.position);
		int cannonNumber = 0;
		if (relativePoint.x < 0.0)
			foreach (Transform cannon in Cannons)
			{
				GameObject cannonBall = Instantiate(ball, Cannons[cannonNumber].position, Cannons[cannonNumber].transform.rotation) as GameObject;
				cannonBall.GetComponent<Rigidbody>().AddForce(-transform.right * Time.deltaTime * 200, ForceMode.Impulse);
				Destroy(cannonBall, 6f);
				GetComponent<AudioSource>().PlayOneShot(cannonSound);
				cannonNumber++;
			}		
		else if (relativePoint.x > 0.0)
			foreach (Transform cannon in Cannons)
			{
				GameObject cannonBall = Instantiate(ball, Cannons[cannonNumber].position, Cannons[cannonNumber].transform.rotation) as GameObject;
				cannonBall.GetComponent<Rigidbody>().AddForce(transform.right*Time.deltaTime*200, ForceMode.Impulse);
				Destroy(cannonBall, 6f);
				GetComponent<AudioSource>().PlayOneShot(cannonSound);
				cannonNumber++;
			}
		reloadAnim.GetComponent<Animation>().Play();
	}

	IEnumerator endCombat()
	{
		yield return new WaitForSeconds(2);
		GetComponent<AudioSource>().clip = defaultBG;
		GetComponent<AudioSource>().Play();
	}

	public void UpdateHealth()
	{
		switch (resManager.shipHullLevel)
		{
			case 1:
				health = 100;
				maxHealth = 100;
				break;
			case 2:
				health = 110;
				maxHealth = 110;
				break;
			case 3:
				health = 120;
				maxHealth = 120;
				break;
			case 4:
				health = 130;
				maxHealth = 130;
				break;
			case 5:
				health = 140;
				maxHealth = 140;
				break;
		}
	}

	public void addCannon()
	{
		Cannons.Add(availableCannons[0]);
		availableCannons.Remove(availableCannons[0]);
	}

	IEnumerator Restart()
	{
		yield return new WaitForSeconds(7);
		Application.LoadLevel(0);
	}
}
