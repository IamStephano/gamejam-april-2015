﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour
{

	private float min = -20.0f;
	private float max = 20.0f;

	// Use this for initialization
	void Start () {
		min = Camera.main.fov + min;
		max = Camera.main.fov + max;
	}
	
	// Update is called once per frame
	void Update () {
		if (Camera.main.fov <= min)
			Camera.main.fov = min;
		if (Camera.main.fov >= max)
			Camera.main.fov = max;
		Camera.main.fov += Input.GetAxis("Mouse ScrollWheel") * -10;	
	}
}
