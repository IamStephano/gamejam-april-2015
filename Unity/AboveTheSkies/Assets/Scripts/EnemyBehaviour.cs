﻿using System;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class EnemyBehaviour : MonoBehaviour
{
	public Transform currentWaypoint;
	public Transform lastWaypoint;
	public bool canMove;
	public bool playerNearby;
	public float speed;

	public GameObject player;
	public Transform target1;
	public Transform target2;
	public GameObject safeDistance;

	public Transform currentTarget;

	public float distance;
	public bool dead;
	public float health = 100;

	public Transform healthbar;

	public float shootTimer;

	public GameObject explosion;
	public GameObject smallExplosion;

	public GameObject ball;
	public GameObject cannon;

	public AudioClip cannonSound;

	public enemySpawn spawner;

	// Use this for initialization
	void Start ()
	{
		spawner = GameObject.Find("GameController").GetComponent<enemySpawn>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		if (health <= 0)
		{
			explosion.SetActive(true);
			healthbar.gameObject.SetActive(false);
			dead = true;
			transform.Translate(-Vector3.up * Time.deltaTime * 5, Space.World);
			transform.Rotate(1, .3f, .1f);
			Destroy(gameObject, 10);
			spawner.piratesAlive.Remove(transform);
		}
		if (!dead)
		{
			healthbar.localScale = new Vector3(health / 100, healthbar.localScale.y, healthbar.localScale.z); 
			if (canMove && currentWaypoint != null && !playerNearby)
			{
				float step = speed*Time.deltaTime;
				Vector3 targetDir = currentWaypoint.transform.position - transform.position;
				Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
				Debug.DrawRay(transform.position, newDir, Color.red);
				transform.rotation = Quaternion.LookRotation(newDir);
				transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.position, step);
				distance = Vector3.Distance(transform.position, currentWaypoint.position);
			}

			if (playerNearby)
			{
				cannon.GetComponent<LookAt>().target = player;
				shootTimer += Time.deltaTime;
				float step = speed*Time.deltaTime;
				float t1Distance = Vector3.Distance(transform.position, target1.transform.position);
				float t2Distance = Vector3.Distance(transform.position, target2.transform.position);
				distance = Vector3.Distance(transform.position, player.transform.position);
				if (distance < 7)
				{
					transform.rotation = Quaternion.Slerp(transform.rotation, player.transform.rotation, Time.deltaTime*0.1f);
					if (shootTimer > 5)
					{
						Fire();
						if (!player.GetComponent<PlayerBehaviour>().inCombat)
						{
							player.GetComponent<PlayerBehaviour>().inCombat = true;
							player.GetComponent<PlayerBehaviour>().enemy = gameObject;
						}
					}
				}
				else
				{

					Vector3 targetDir = player.transform.position - transform.position;
					Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
					Debug.DrawRay(transform.position, newDir, Color.red);
					transform.rotation = Quaternion.LookRotation(newDir);

				}
				if (distance > 4f)
				{
					if (t1Distance < t2Distance)
					{
						currentTarget = target1;
					}
					else
					{
						currentTarget = target2;
					}
					transform.position = Vector3.MoveTowards(transform.position, currentTarget.transform.position, step*0.9f);
				}
				//transform.position = Vector3.MoveTowards(safeDistance.transform.position, player.transform.position, step);
			}
			if (distance < 0.1f && canMove && !playerNearby)
			{
				nextWaypoint();
			}
		}
	}

	public void nextWaypoint()
	{

		canMove = false;
		Debug.Log("finding next waypoint");
		List<Collider> nearbyWaypoints = new List<Collider>();
		Collider[] hitWayPoints = Physics.OverlapSphere(gameObject.transform.position, 10);
		foreach (Collider hit in hitWayPoints)
		{
			Debug.Log("checking collider");
			if (hit.tag == "Player")
			{
				playerNearby = true;
			}
			if (hit.tag == "Waypoint")
			{
				Debug.Log("Waypoint Nearby");
				nearbyWaypoints.Add(hit);
			}
		}
		int currentWaypointNumber = Random.Range(0, nearbyWaypoints.Count);
		if (nearbyWaypoints[currentWaypointNumber] == currentWaypoint || nearbyWaypoints[currentWaypointNumber] == lastWaypoint)
		{
			nextWaypoint();
		}
		else
		{
			lastWaypoint = currentWaypoint;
			currentWaypoint = nearbyWaypoints[currentWaypointNumber].transform;
		}
		canMove = true;
		
	}

	void FixedUpdate()
	{
		if (!playerNearby)
		{
			Collider[] hitWayPoints = Physics.OverlapSphere(gameObject.transform.position, 10);
			foreach (Collider hit in hitWayPoints)
			{
				if (hit.tag == "Player")
				{
					playerNearby = true;
				}
			}
		}
	}

	void Fire()
	{
		GetComponent<AudioSource>().PlayOneShot(cannonSound);
		player.GetComponent<PlayerBehaviour>().health -= 15;
		GameObject clone = Instantiate(smallExplosion, player.transform.position, Quaternion.identity) as GameObject;
		Destroy(clone, 3);
		GameObject cannonBall = Instantiate(ball, transform.position, cannon.transform.rotation) as GameObject;
		cannonBall.GetComponent<Rigidbody>().AddForce(transform.right * Time.deltaTime * 700, ForceMode.Impulse);
		Destroy(cannonBall, 1);
		shootTimer = 0;
	}
}
