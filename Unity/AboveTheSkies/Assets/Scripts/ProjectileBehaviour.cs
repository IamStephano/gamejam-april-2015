﻿using UnityEngine;
using System.Collections;

public class ProjectileBehaviour : MonoBehaviour {

	// Use this for initialization
	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Pirate")
		{
			other.gameObject.GetComponent<EnemyBehaviour>().health -= 20;
		}
		Destroy(gameObject);
	}
}
