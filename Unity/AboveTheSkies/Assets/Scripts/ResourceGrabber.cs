﻿using UnityEngine;
using System.Collections;

public class ResourceGrabber : MonoBehaviour
{

	private ResourceContainer resManager;
	public Texture2D handCursor;
	


	void Start()
	{
		resManager = GameObject.Find("GameController").GetComponent<ResourceContainer>();
	}
	void OnMouseDown()
	{
	    if (Vector3.Distance(transform.position, GameObject.FindGameObjectWithTag("GameController").transform.position) < 20)
	    {
            if (GetComponent<ResourceSpawner>().res > 0)
            {
                switch (GetComponent<ResourceSpawner>().resource)
                {
                    case "lumber":
                        resManager.GetRes(0,1,0,0,0);
                        GetComponent<ResourceSpawner>().res--;
                        break;
                    case "ore":
                        resManager.GetRes(1,0,0,0,0);
                        GetComponent<ResourceSpawner>().res--;
                        break;
                    case "gems":
                        resManager.GetRes(0,0,0,1,0);
                        GetComponent<ResourceSpawner>().res--;
                        break;
                    case "wool":
                        resManager.GetRes(0,0,1,0,0);
                        GetComponent<ResourceSpawner>().res--;
                        break;
                }
                //GetComponent<ResourceSpawner>().res = 0;
            }
	    }

	}

	void OnMouseEnter()
	{
		Cursor.SetCursor(handCursor, Vector2.zero, CursorMode.Auto);
	}

	void OnMouseExit()
	{
		Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
	}
	
}
