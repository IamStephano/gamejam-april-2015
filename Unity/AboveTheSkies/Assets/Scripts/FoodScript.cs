﻿using UnityEngine;
using System.Collections;

public class FoodScript : MonoBehaviour 
{
    public GameObject foodScript;
    public GameObject playerHealth;
    float timer = 15f;  

	void Start () 
    {
          
	}
	
	void Update () 
    {
        if (foodScript.GetComponent<ResourceContainer>().playerFood > 0)
        {
            
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                foodScript.GetComponent<ResourceContainer>().playerFood -= 1;
                timer = 10f;
            }
        }
        else
        {
            playerHealth.GetComponent<PlayerBehaviour>().health -= Time.deltaTime*3;
        }
	}
}
