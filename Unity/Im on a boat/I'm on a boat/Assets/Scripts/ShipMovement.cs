﻿using UnityEngine;
using System.Collections;

public class ShipMovement : MonoBehaviour {

	
	//DONT USE THIS SCRUPT!!!!
	
	public float rotationSpeed;
	public float translationSpeed;

	void Update()
	{
		float rotation = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
		float translation = Input.GetAxis("Vertical") * translationSpeed * Time.deltaTime;

		GetComponent<Rigidbody>().AddForce(-transform.right * translation * 100);
		GetComponent<Rigidbody>().AddTorque(transform.up * rotation * 100);
	}
}

