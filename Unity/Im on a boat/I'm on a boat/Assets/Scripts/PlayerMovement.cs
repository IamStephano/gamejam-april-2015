﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    public float shipSpeed;
    public int maneuverability;

    private Rigidbody rb;

	void Start ()
	{
	    rb = GetComponent<Rigidbody>();
	}

    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0,-1,0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 1, 0);
        }
    }
	
	void FixedUpdate ()
	{
	    float shipDir = transform.eulerAngles.y;
	    if (shipDir > 180)
	    {
	        shipDir = 360 - shipDir;
	    }

	    float degreesFrom0 = Subtract(WindBehaviour.windDirection, shipDir);
	    if (degreesFrom0 > maneuverability)
	    {
	        degreesFrom0 = maneuverability;
	    }
	    degreesFrom0 = maneuverability - degreesFrom0;

        float windModifier = degreesFrom0/maneuverability;

	    float speed = WindBehaviour.windSpeed*windModifier*shipSpeed;
        rb.AddForce(-transform.right*speed);
	    if (rb.velocity.x > speed)
	    {
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, speed);
	    }
        
	}

    float Subtract(float n1, float n2)
    {
        if (n1 > n2)
        {
            //Debug.Log("n1 "+n1+" | n2 "+n2+" | sum "+(n1-n2));
            return n1 - n2;
        }
        else
        {
            //Debug.Log("n1 " + n1 + " | n2 " + n2 + " | sum " + (n2 - n1));
            return n2 - n1;
        }
        
    }
}
